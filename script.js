const imageContainer = document.getElementById('image-container');
const loader = document.getElementById('loader');

let ready = false;
let imagesLoaded = 0;
let totalImages = 0;
let photosArray = [];
let isInitialLoad = true;

// Unspash API
let initialCount = 5;
const apiKey = 'V6WIhBPT0dibdc5Za2bcxi8iHpkG1nf4kyJXjSSetw0';
let apiUrl = `https://api.unsplash.com/photos/random/?client_id=${apiKey}&count=${initialCount}`;

function updateAPIURLWithNewCount(imageCount) {
	apiUrl = `https://api.unsplash.com/photos/random/?client_id=${apiKey}&count=${imageCount}`;
}

// Check is all images were loaded
function imageLoaded() {
	imagesLoaded++;
	if (imagesLoaded === totalImages) {
		ready = true;
		loader.hidden = true;
		count = 10;
	}
}

async function getPhotosFromUnspashed() {
	try {
		const response = await fetch(apiUrl);
		photosArray = await response.json();
		displayPhotos();
		if (isInitialLoad) {
			updateAPIURLWithNewCount(10);
			isInitialLoad = false;
		}
	} catch (error) {
		console.log('Error fetching images', error);
	}
}

// Helper function to Set Attributes on DOM elements
function setAttributes(element, attributes) {
	for (const key in attributes) {
		element.setAttribute(key, attributes[key]);
	}
}

// Create elements for links and photos and add to DOM
function displayPhotos() {
	imagesLoaded = 0;
	totalImages = photosArray.length;
	// Run function for each object in photosArray
	photosArray.forEach((photo) => {
		// Create <a> to link to Unspash
		const item = document.createElement('a');

		setAttributes(item, { href: photo.links.html, target: '_blank' });

		// Create <img> for photo
		const img = document.createElement('img');
		setAttributes(img, {
			src: photo.urls.regular,
			alt: photo.alt_description,
			title: photo.alt_description,
		});

		// Event Listener, check when each is finished loading
		img.addEventListener('load', imageLoaded);

		// Put <img> inside <a>, then put both inside imageContainer element
		item.appendChild(img);
		imageContainer.appendChild(item);
	});
}

// Check to see if scrolling near botton of page, Load more photos
window.addEventListener('scroll', () => {
	if (
		window.innerHeight + window.scrollY >= document.body.offsetHeight - 1000 &&
		ready
	) {
		ready = false;
		getPhotosFromUnspashed();
	}
});

// On load
getPhotosFromUnspashed();
